const express = require("express");

const PostController = require("../controllers/posts");
const checkAuth = require('../middleware/check-auth');
const extractFile = require('../middleware/file');

const router = express.Router();



// post
router.post('', checkAuth, extractFile, PostController.createPost);

// update
router.put(
    '/:id',
    checkAuth,
    extractFile,
    PostController.updatePost);

router.get('/:id', PostController.getPost)

// get
router.get('', PostController.getPosts);

// Delete
router.delete('/:id', checkAuth, PostController.deletePost);

module.exports = router;