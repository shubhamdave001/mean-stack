const express = require('express');
const bodyParser = require('body-parser');
const path = require("path");
const mongoose = require('mongoose');

const postsRoutes = require("./routes/posts");

const usersRoutes = require("./routes/users");

const app = express();

// DB CONNECTION
mongoose
    .connect(
        "mongodb+srv://shubham:" + process.env.MONGO_ATLAS_PW + "@cluster0.cfyca.mongodb.net/MEAN?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(console.log("Connected to DB"))
    .catch((err) => console.log(err, 'Error Occured'));

app.use(bodyParser.json());

app.use("/images", express.static(path.join("images")));

// Set Static access for angular
app.use("/", express.static(path.join(__dirname, "angular")));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, PUT, DELETE, OPTIONS"
    )
    next();
});

app.use("/api/posts", postsRoutes);

app.use("/api/user", usersRoutes);

app.use((req, res, next) => {
    res.sendFile(path.join(__dirname, "angular", "index.html"));
});

module.exports = app;