import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthServiceService } from '../auth/auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  userAuthenticated: boolean = false;
  private authListerSubs: Subscription;

  constructor(private authService: AuthServiceService) { }

  ngOnInit(): void {
    this.userAuthenticated = this.authService.getIsAuth();
    this.authListerSubs = this.authService.getAuthStatusLister().subscribe(isAuthenticated => {
      this.userAuthenticated = isAuthenticated;
    });
  }

  ngOnDestroy() {
    this.authListerSubs.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
  }
}
