import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { AuthServiceService } from 'src/app/auth/auth-service.service';

import { Post } from "../post.model";
import { PostsService } from "../post.service";
@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  // posts = [
  //   { title: "First Title", body: "First Body" },
  //   { title: "Second Title", body: "Second Body" },
  //   { title: "Third Title", body: "Third Body" },
  //   { title: "Fourth Title", body: "Fourth Body" },
  // ]
  isLoading = false;
  posts: Post[] = [];
  totalPost = 0;
  postPerPage = 2;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  private userAuthenticated: boolean = false;
  private postsSub: Subscription;
  private authListnerSub: Subscription;
  private userId: string;
  constructor(
    public postService: PostsService,
    private authService: AuthServiceService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.postService.getPost(this.postPerPage, this.currentPage);
    this.userId = this.authService.getUserId();
    this.postsSub = this.postService.getPostUpdateListner()
      .subscribe((postData: { posts: Post[], postCount: number }) => {
        this.isLoading = false;
        // console.log(this.posts, posts);
        this.posts = postData.posts;
        this.totalPost = postData.postCount;
      });
    this.userAuthenticated = this.authService.getIsAuth();
    this.authService.getAuthStatusLister().subscribe(isAuthenticated => {
      this.userAuthenticated = isAuthenticated;
      this.userId = this.authService.getUserId();
    })

  }
  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    console.log(pageData);
    this.postPerPage = pageData.pageSize;
    this.currentPage = pageData.pageIndex + 1;
    this.postService.getPost(this.postPerPage, this.currentPage);

  }
  onDelete(postId: string) {
    this.isLoading = true;
    this.postService.deletePost(postId).subscribe(() => {
      this.postService.getPost(this.postPerPage, this.currentPage);
    }, () => {
      this.isLoading = false;
    })
  }
  ngOnDestroy() {
    this.postsSub.unsubscribe();
  }
}
