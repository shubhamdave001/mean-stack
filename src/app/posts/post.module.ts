import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PostCreateComponent } from './post-create/post-create.component';
import { PostListComponent } from './post-list/post-list.component';
import { AngularMaterialModule } from "../angular-material.module";
import { HomeComponent } from './home/home.component';


@NgModule({
    declarations: [
        PostCreateComponent,
        PostListComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        AngularMaterialModule
    ]
})
export class PostsModule { }