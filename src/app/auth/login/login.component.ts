import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthServiceService } from '../auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  isLoading = false;
  authStatusSub: Subscription;
  constructor(
    private authService: AuthServiceService
  ) { }

  ngOnInit(): void {
    this.authStatusSub = this.authService.getAuthStatusLister().subscribe(authStatus => {
      console.log(authStatus);

      this.isLoading = false
    })
  }
  onLogin(form: NgForm) {
    console.log(form.value);
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    this.authService.loginUser(form.value.email, form.value.password)
  }
  ngOnDestroy() {

  }
}
