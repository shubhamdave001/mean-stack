import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

import { AuthServiceService } from "../auth-service.service";
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  isLoading = false;
  authStatusSub: Subscription;
  constructor(
    public authService: AuthServiceService
  ) { }

  ngOnInit(): void {
    this.authStatusSub = this.authService.getAuthStatusLister().subscribe(authStatus => {
      console.log(authStatus);

      this.isLoading = false;
    })
  }
  onSignUp(form: NgForm) {
    console.log(form.value);
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    this.authService.createUser(form.value.email, form.value.password)
  }
  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
